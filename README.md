# Manualetto Software RadioHam

Opuscolo sul Software Libero indirizzato all'uso radioamatoriale.
Stampabile su un singolo A4 fronte retro.

## Istruzioni

- Installare LaTeX
- Lanciare ```make booklet```
- Stampare il file dist/main-book.pdf
