## Reduced version of TEX makefile

### Select all raster ###
RAST_DIR = ./img
RASTF    = $(wildcard $(RAST_DIR)/*)

all: dist/main.pdf

booklet: dist/main.pdf
	@pdfbook --outfile dist $<

dist/%.pdf: build/%.pdf
	mv $< $@

build/%.pdf: %.tex $(RASTF) makefile
	@mkdir -p build
	@mkdir -p dist
	@if [ -t 1 ]; then echo -e "\033[1;33m[Mashup      ]\033[0m main.tex"; else echo "Mashup"; fi
	@pdflatex -output-directory build -synctex=1 -interaction=nonstopmode $< > /dev/null
	@pdflatex -output-directory build -synctex=1 -interaction=nonstopmode $< > /dev/null

clean:
	rm -rf build
	rm -rf $(VEC_DIR)
